import logging
import os
import telegram
from telegram.ext import (Updater, CommandHandler)
import requests
from bs4 import BeautifulSoup

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

TOKEN = '5711526196:AAHds36iCY9bm1d2uZkRSawGLErXeATy03M'
bot = telegram.Bot(token=TOKEN)
PORT = int(os.environ.get('PORT', 5000))

def start(update):
    update.message.reply_text('Hi, this bot send you some news:')

def readurl(update, context):
    with open("list-url.txt", "r") as f:
        content = f.read()
        list_urls = content.split("\n")
        update.message.reply_text('list of the urls is :' + list_urls )
def writenews(update, context):
    for i in range(6) :
        if list_urls[i] == '':
            break
        print(list_urls[i])
        SourcePage = requests.get(list_urls[i]).text
        SoupPage = BeautifulSoup(SourcePage, 'html.parser')
        title = SoupPage.find("h1",{"class":'article__title'}).text
        print(title)
        bodyText = SoupPage.find("div",{"class":'column is-10-desktop is-offset-1-desktop is-8-widescreen is-offset-2-widescreen'}).text
        print(bodyText)

def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the coneversation", user.first_name)
    update.message.reply_text("by")

def main():
    updater = Updater(TOKEN)
    dp = updater.dispatcher
    updater.start_webhook(listen='0.0.0.0', port=PORT, url_path=TOKEN)
    updater.bot.set_webhook('https://ee08-5-236-254-125.eu.ngrok.io/' + TOKEN)
    updater.idle()

if __name__ == '__main__':
    main()
