import requests
from bs4 import BeautifulSoup
import logging
import os
import telegram
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, ConversationHandler)

url = ""

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

TOKEN = '5711526196:AAHds36iCY9bm1d2uZkRSawGLErXeATy03M'
bot = telegram.Bot(token=TOKEN)
PORT = int(os.environ.get('PORT', 5000))

URL, TITLE, BODY = range(3)

def start(update):
    update.message.reply_text('Hi, send your url:')
    
    return URL

def url(update):
   
    url = update.message.text
    logger.info(" your url is: %s", update.message.text)
    SourcePage = requests.get(url)
    SoupPage = BeautifulSoup(SourcePage.text, 'html.parser')
   
    return TITLE

def title(update):
   
    title = SoupPage.head.title
    print(title.text)
    return BODY

def body(update):
   
    bodyText = SoupPage.find("div",{"class":'column is-10-desktop is-offset-1-desktop is-8-widescreen is-offset-2-widescreen'})
    print(bodyText.text)
   
    return ConversationHandler.END

def cancel(update, context):
   
    user = update.message.from_user
    logger.info("User %s canceled the coneversation", user.first_name)
    update.message.reply_text("by")
    
    return ConversationHandler.END

def main():
    updater = Updater(TOKEN)
    dp = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points = [CommandHandler('start', start)],

        states={
            URL:[CommandHandler('start', start), MessageHandler(Filters.text, url)],
            TITLE:[CommandHandler('start', start), MessageHandler(Filters.text, title)],
            BODY:[CommandHandler('start', start), MessageHandler(Filters.text, body)],
            },
        fallbacks=[CommandHandler('cancle', cancel)]
    )

    dp.add_handler(conv_handler)
    updater.start_webhook(listen='0.0.0.0', port=PORT, url_path=TOKEN)
    updater.bot.set_webhook('https://ee08-5-236-254-125.eu.ngrok.io/' + TOKEN)
    updater.idle()

if __name__ == '__main__':
    main()
