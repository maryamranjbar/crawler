import requests
from bs4 import BeautifulSoup


url = "https://www.caritas.lu/en/caritas-news/actualites/regular-donations-guarantee-effectiveness-our-actions"
SourcePage = requests.get(url)

SoupPage = BeautifulSoup(SourcePage.text, 'html.parser')

title = SoupPage.head.title
print(title.text)

bodyText = SoupPage.find("div",{"class":'column is-10-desktop is-offset-1-desktop is-8-widescreen is-offset-2-widescreen'})
print(bodyText.text)
