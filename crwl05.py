import requests
from bs4 import BeautifulSoup
import sqlite3

#data base
conn = sqlite3.connect('news.db')
cur = conn.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS news( newsid INT PRIMARY KEY, titr TEXT, matn TEXT)")
conn.commit()

with open("list-url.txt", "r") as f:
    content = f.read()
    list_urls = content.split("\n")

for i in range(6) :
    if list_urls[i] == '':
        break
    SourcePage = requests.get(list_urls[i]).text
    SoupPage = BeautifulSoup(SourcePage, 'html.parser')
    title = SoupPage.find("h1",{"class":'article__title'}).text
    bodyText = SoupPage.find("div",{"class":'column is-10-desktop is-offset-1-desktop is-8-widescreen is-offset-2-widescreen'}).text
    entity =(i, title, bodyText)
    cur.execute("""INSERT INTO news VALUES(?, ?, ?)""", entity)
    conn.commit()
cur.execute("SELECT titr,matn FROM news WHERE 0=<newsid<5")
print(cur.fetchall())
